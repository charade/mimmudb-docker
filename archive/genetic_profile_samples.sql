/*
 Navicat MySQL Data Transfer

 Source Server         : cbio_user@localhost.cbioportal
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost
 Source Database       : cgds_test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : utf-8

 Date: 03/21/2019 20:09:16 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `genetic_profile_samples`
-- ----------------------------
DROP TABLE IF EXISTS `genetic_profile_samples`;
CREATE TABLE `genetic_profile_samples` (
  `GENETIC_PROFILE_ID` int(11) NOT NULL,
  `ORDERED_SAMPLE_LIST` longtext NOT NULL,
  UNIQUE KEY `GENETIC_PROFILE_ID` (`GENETIC_PROFILE_ID`),
  CONSTRAINT `genetic_profile_samples_ibfk_1` FOREIGN KEY (`GENETIC_PROFILE_ID`) REFERENCES `genetic_profile` (`GENETIC_PROFILE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `genetic_profile_samples`
-- ----------------------------
BEGIN;
INSERT INTO `genetic_profile_samples` VALUES ('2', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,'), ('3', '2,3,6,8,9,10,12,13,'), ('4', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,'), ('5', '2,');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
