/*
 Navicat MySQL Data Transfer

 Source Server         : cbio_user@localhost.cbioportal
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost
 Source Database       : cgds_test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : utf-8

 Date: 03/21/2019 18:34:12 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `genetic_entity`
-- ----------------------------
DROP TABLE IF EXISTS `genetic_entity`;
CREATE TABLE `genetic_entity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=579 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `genetic_entity`
-- ----------------------------
BEGIN;
INSERT INTO `genetic_entity` VALUES ('1', 'GENE'), ('2', 'GENE'), ('3', 'GENE'), ('4', 'GENE'), ('5', 'GENE'), ('6', 'GENE'), ('7', 'GENE'), ('8', 'GENE'), ('9', 'GENE'), ('10', 'GENE'), ('11', 'GENE'), ('12', 'GENE'), ('13', 'GENE'), ('14', 'GENE'), ('15', 'GENE');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
