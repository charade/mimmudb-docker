/*
 Navicat MySQL Data Transfer

 Source Server         : cbio_user@localhost.cbioportal
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost
 Source Database       : cgds_test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : utf-8

 Date: 03/21/2019 19:13:18 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `genetic_profile`
-- ----------------------------
DROP TABLE IF EXISTS `genetic_profile`;
CREATE TABLE `genetic_profile` (
  `GENETIC_PROFILE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `STABLE_ID` varchar(255) NOT NULL,
  `CANCER_STUDY_ID` int(11) NOT NULL,
  `GENETIC_ALTERATION_TYPE` varchar(255) NOT NULL,
  `DATATYPE` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` mediumtext,
  `SHOW_PROFILE_IN_ANALYSIS_TAB` tinyint(1) NOT NULL,
  PRIMARY KEY (`GENETIC_PROFILE_ID`),
  UNIQUE KEY `STABLE_ID` (`STABLE_ID`),
  KEY `CANCER_STUDY_ID` (`CANCER_STUDY_ID`),
  CONSTRAINT `genetic_profile_ibfk_1` FOREIGN KEY (`CANCER_STUDY_ID`) REFERENCES `cancer_study` (`CANCER_STUDY_ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `genetic_profile`
-- ----------------------------
BEGIN;
INSERT INTO `genetic_profile` VALUES ('2', 'study_tcga_pub_gistic', '1', 'COPY_NUMBER_ALTERATION', 'DISCRETE', 'Putative copy-number alterations from GISTIC', 'Putative copy-number from GISTIC 2.0. Values: -2 = homozygous deletion; -1 = hemizygous deletion; 0 = neutral / no change; 1 = gain; 2 = high level amplification.', '1'), ('3', 'study_tcga_pub_mrna', '1', 'MRNA_EXPRESSION', 'Z-SCORE', 'mRNA expression (microarray)', 'Expression levels (Agilent microarray).', '0'), ('4', 'study_tcga_pub_log2CNA', '1', 'COPY_NUMBER_ALTERATION', 'LOG2-VALUE', 'Log2 copy-number values', 'Log2 copy-number values for each gene (from Affymetrix SNP6).', '0'), ('5', 'study_tcga_pub_methylation_hm27', '1', 'METHYLATION', 'CONTINUOUS', 'Methylation (HM27)', 'Methylation beta-values (HM27 platform). For genes with multiple methylation probes, the probe least correlated with expression is selected.', '0'), ('6', 'study_tcga_pub_mutations', '1', 'MUTATION_EXTENDED', 'MAF', 'Mutations', 'Mutation data from whole exome sequencing.', '1'), ('7', 'study_tcga_pub_sv', '1', 'STRUCTURAL_VARIANT', 'SV', 'Structural Variants', 'Structural Variants detected by Illumina HiSeq sequencing.', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
