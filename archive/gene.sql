/*
 Navicat MySQL Data Transfer

 Source Server         : cbio_user@localhost.cbioportal
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost
 Source Database       : cgds_test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : utf-8

 Date: 03/21/2019 19:12:24 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `gene`
-- ----------------------------
DROP TABLE IF EXISTS `gene`;
CREATE TABLE `gene` (
  `ENTREZ_GENE_ID` int(11) NOT NULL,
  `HUGO_GENE_SYMBOL` varchar(255) NOT NULL,
  `GENETIC_ENTITY_ID` int(11) NOT NULL,
  `TYPE` varchar(50) DEFAULT NULL,
  `CYTOBAND` varchar(64) DEFAULT NULL,
  `LENGTH` int(11) DEFAULT NULL,
  PRIMARY KEY (`ENTREZ_GENE_ID`),
  UNIQUE KEY `GENETIC_ENTITY_ID_UNIQUE` (`GENETIC_ENTITY_ID`),
  KEY `HUGO_GENE_SYMBOL` (`HUGO_GENE_SYMBOL`),
  CONSTRAINT `gene_ibfk_1` FOREIGN KEY (`GENETIC_ENTITY_ID`) REFERENCES `genetic_entity` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `gene`
-- ----------------------------
BEGIN;
INSERT INTO `gene` VALUES ('207', 'AKT1', '1', 'protein-coding', '14q32.32', '10838'), ('208', 'AKT2', '2', 'protein-coding', '19q13.1-q13.2', '15035'), ('369', 'ARAF', '4', 'protein-coding', 'Xp11.4-p11.2', '3204'), ('472', 'ATM', '5', 'protein-coding', '11q22-q23', '22317'), ('672', 'BRCA1', '7', 'protein-coding', '17q21', '8426'), ('673', 'BRAF', '6', 'protein-coding', '7q34', '4564'), ('675', 'BRCA2', '8', 'protein-coding', '13q12.3', '11269'), ('983', 'CDK1', '14', 'protein-coding', '10q21.1', '3931'), ('3265', 'HRAS', '9', 'protein-coding', '11p15.5', '1854'), ('3845', 'KRAS', '10', 'protein-coding', '12p12.1', '7302'), ('4893', 'NRAS', '11', 'protein-coding', '1p13.2', '4449'), ('8085', 'KMT2D', '15', 'protein-coding', '12q13.12', '20476'), ('10000', 'AKT3', '3', 'protein-coding', '1q44', '7499'), ('51259', 'TMEM216', '12', 'protein-coding', '11q13.1', '2364'), ('282770', 'OR10AG1', '13', 'protein-coding', '11q11', '906');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
