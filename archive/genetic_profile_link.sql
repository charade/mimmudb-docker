/*
 Navicat MySQL Data Transfer

 Source Server         : cbio_user@localhost.cbioportal
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost
 Source Database       : cgds_test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : utf-8

 Date: 03/21/2019 20:09:07 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `genetic_profile_link`
-- ----------------------------
DROP TABLE IF EXISTS `genetic_profile_link`;
CREATE TABLE `genetic_profile_link` (
  `REFERRING_GENETIC_PROFILE_ID` int(11) NOT NULL,
  `REFERRED_GENETIC_PROFILE_ID` int(11) NOT NULL,
  `REFERENCE_TYPE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`REFERRING_GENETIC_PROFILE_ID`,`REFERRED_GENETIC_PROFILE_ID`),
  KEY `REFERRED_GENETIC_PROFILE_ID` (`REFERRED_GENETIC_PROFILE_ID`),
  CONSTRAINT `genetic_profile_link_ibfk_1` FOREIGN KEY (`REFERRING_GENETIC_PROFILE_ID`) REFERENCES `genetic_profile` (`GENETIC_PROFILE_ID`) ON DELETE CASCADE,
  CONSTRAINT `genetic_profile_link_ibfk_2` FOREIGN KEY (`REFERRED_GENETIC_PROFILE_ID`) REFERENCES `genetic_profile` (`GENETIC_PROFILE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
